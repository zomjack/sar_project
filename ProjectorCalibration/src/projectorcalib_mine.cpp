#include "cameraconverter.cpp"

#ifdef __WIN32__
#include <Windows.h>
#endif
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>



#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;

const static string RGBparams = "kinectrgb.xml";
const static string IRparams = "kinectir.xml";
const static string Stereoparams = "kinectstereo.xml";

bool calibrated = false;

Mat cameraMatrixRGB, distCoeffsRGB;
Mat cameraMatrixIR, distCoeffsIR;
Mat cameraMatrixProj, distCoeffsProj;
Mat rotationMat, translationMat;

vector<Point3d> depthWorldCoords;

vector<vector<Point3f> > cornerWorldPoints;
vector<vector<Point2f> > cornerPatternPoints;
vector<vector<Point2f> > rgbCornerPoints;

double fx_depthcam, fy_depthcam, fx_rgbcam, fy_rgbcam;
double cx_depthcam, cy_depthcam, cx_rgbcam, cy_rgbcam;

const int PROJ_WIDTH_LOW_RES = 1024;
const int PROJ_HEIGHT_LOW_RES = 768;
const int PROJ_WIDTH_HIGH_RES = 1920;
const int PROJ_HEIGHT_HIGH_RES = 1080;

int chessboardOffsetX = 20; // default
int chessboardOffsetY = 20; // default

int squareSize = 100; // default
const int squareSizeMax = 100;

Size patternSize(9, 6);

const int chessboardOffsetMaxX = PROJ_WIDTH_LOW_RES - (patternSize.width + 1)*squareSize;
const int chessboardOffsetMaxY = PROJ_HEIGHT_LOW_RES - (patternSize.height + 1)*squareSize;


int chessboardBGColor = 200; 

Mat DepthMatDisp = Mat(424, 512, CV_16UC1);

Mat finalColorpic = Mat(360, 640, CV_8UC4) = {0};

Mat pattern_lowres = Mat(PROJ_HEIGHT_LOW_RES, PROJ_WIDTH_LOW_RES, CV_8UC1, chessboardBGColor);
Mat pattern_highres = Mat(PROJ_HEIGHT_HIGH_RES, PROJ_WIDTH_HIGH_RES, CV_8UC1, chessboardBGColor);

const static string kinectProjectorIntrinsics = "projector_intrinsics.xml";
const static string kinectProjectorExtrinsics = "projector_extrinsics.xml";

vector<Point2f> rgbPointsMapped;

Mat kinectRGBRaw = Mat(360, 640, CV_8UC4);
Mat kinectDepthRaw = Mat(424, 512, CV_16UC1);

cameraconverter camconverter;

void oldAlignImages(cv::Mat& finalcolorpic, vector<Mat> rgbir) {
	//printf("insi-de align images");
	//Mat a = rgbir.at(0);
	//cout << a << endl;
	//Sleep(2000);
	Mat pDepth = rgbir.at(1);
	Mat pRGB = rgbir.at(0);
	int height = 424;
	int width = 512;
	cv::Mat_<float> pt(3, 1), rotationMat(3, 3), translationMat(3, 1);
	cv::Mat_<float> cameraIRMat(3, 3), cameraRGBMat(3, 3), fundamentalMat(3, 3);
	double fx_depthcam = 3.4911773785848590e+02;
	double fy_depthcam = 3.4911773785848590e+02;
	double fx_rgbcam = 3.4911773785848590e+02;
	double fy_rgbcam = 3.4911773785848590e+02;
	double cx_depthcam = 2.5550000000000000e+02;
	double cy_depthcam = 2.1150000000000000e+02;
	double cx_rgbcam = 2.5550000000000000e+02;
	double cy_rgbcam = 2.1150000000000000e+02;

	//undistort(depthmat, undistortir, cameraMatrixIR, distCoeffsIR);

	//cameraIRMat = (cv::Mat_<double_t>(3, 3) << 3.4911773785848590e+02, 0.0, 2.5550000000000000e+02, 0.0,3.4911773785848590e+02,2.1150000000000000e+02, 0.0, 0.0, 1.0);
	//cameraRGBMat = (cv::Mat_<double_t>(3, 3) << 3.3453930899418316e+02, 0.0, 3.1950000000000000e+02, 0.0, 3.3453930899418316e+02, 1.7950000000000000e+02, 0.0, 0.0, 1.0);
	//fundamentalMat = (cv::Mat_<double_t>(3, 3) << 2.8106982221197487e-07, 3.8800040617688400e-05, - 5.9772657468657149e-03, - 3.4693101408160577e-05, - 3.1044541334586553e-06 - 7.7192116370153399e-02, 6.7748662639252524e-03, 8.3258034041477663e-02, 1.0);
	rotationMat = (cv::Mat_<double_t>(3, 3) << 9.9984474507650234e-01, -8.5227423060502804e-03, 1.5422341148111264e-02, 8.7209248152820985e-03, 9.9987967317445114e-01, -1.2829054638548786e-02, -1.5311146700043797e-02, 1.2961559942280636e-02, 9.9979876312715676e-01);
	translationMat = (cv::Mat_<double_t>(3, 1) << -5.3474633389422998e+01, -7.5021683895586644e-01, 7.8208269240532591e+00);

	for (int row = 0; row<height; ++row)
	{
		for (int col = 0; col<width; ++col)
		{

			float depth = pDepth.at<float>(row, col);
			//cv::imshow("IR", rgbir.at(1));
			//printf("depth rows -->%d depth cols -->%d", pDepth.rows, pDepth.cols);
			//if (depth > 0.1) {

			//Sleep(10000);
			// Map depthcam depth to 3D point
			pt(0) = depth*(row - cx_depthcam) / fx_depthcam;  // No need for a 3D point buffer
			pt(1) = depth*(col - cy_depthcam) / fy_depthcam;  // here, unless you need one.
			pt(2) = depth;



			//pt=rotationMat.inv()*pt-translationMat;
			pt = rotationMat*pt + translationMat;


			// Project 3D point to rgbcam
			float x_rgbcam = fx_rgbcam*pt(0) / pt(2) + cx_rgbcam;
			float y_rgbcam = fy_rgbcam*pt(1) / pt(2) + cy_rgbcam;

			//printf("Value of x_rgbcam %f", x_rgbcam);
			//printf("Value of y_rgbcam %f", y_rgbcam);

			int px_rgbcam = 0;
			int py_rgbcam = 0;


			if (x_rgbcam >= 0 && y_rgbcam >= 0 && x_rgbcam < 640 && y_rgbcam < 360) {
				// "Interpolate" pixel coordinates (Nearest Neighbors, as discussed above)
				px_rgbcam = cvRound(x_rgbcam);
				py_rgbcam = cvRound(y_rgbcam);
				//printf("valx%dvaly%d", px_rgbcam, py_rgbcam);
			}

			Vec4b temp;
			//finalcolorpic.at<uchar>(px_rgbcam, py_rgbcam) = 100;
			//printf("Value of PXPX %d", px_rgbcam);
			//printf("Value of PYPY %d", py_rgbcam);
			//printf("Value of temp %f", temp);
			//temp = pRGB.at<uchar>(py_rgbcam, px_rgbcam);					
			/*
			if (pRGB.at<uchar>(py_rgbcam, px_rgbcam) < 1000) {
			printf("thhhhhh");
			}*/
			temp = pRGB.at<Vec4b>(py_rgbcam, px_rgbcam);
			/*		if (temp < 1000) {
			printf("thomas");
			printf("%u", temp);
			printf("john");
			}*/
			finalcolorpic.at<Vec4b>(py_rgbcam, px_rgbcam) = temp;



			//finalcolorpic(px_rgbcam,py_rgbcam) = pRgb.at<uchar>(px_rgbcam, py_rgbcam);
			//printf("px_rgbcam: %d py_rgbcam %ld\n", px_rgbcam, py_rgbcam);
			//printf("val: %d \n", pRGB.at<uchar>(row, col));

		}

	}
}


vector<Mat> nextRGBDImagePair() {
	Mat rgb;
	Mat d;
	vector<Mat> rgbDepth = vector<Mat>();
	camconverter.getRGBFeed(kinectRGBRaw);
	
	rgbDepth.push_back(kinectRGBRaw);

	camconverter.getDepthFrame(kinectDepthRaw);
	rgbDepth.push_back(kinectDepthRaw);

	return rgbDepth;
}

void showCalibrationPattern() {

	pattern_lowres = Mat(PROJ_HEIGHT_LOW_RES, PROJ_WIDTH_LOW_RES, CV_8UC1, chessboardBGColor);

	int maxPatternHeight = chessboardOffsetY + squareSize * (patternSize.height + 1);
	int maxPatternWidth = chessboardOffsetX + squareSize * (patternSize.width + 1);

	if (maxPatternHeight > PROJ_HEIGHT_LOW_RES || maxPatternWidth > PROJ_WIDTH_LOW_RES) {
		cerr << "Chessboard pattern values are out of range!\n";
		return;
	}

	bool blackY = false;
	for (int y = chessboardOffsetY; y < maxPatternHeight; y++) {
		bool blackX = false;
		if (((y - chessboardOffsetY) % squareSize) == 0)
			blackY = !blackY;

		blackX = blackY;

		for (int x = chessboardOffsetX; x < maxPatternWidth; x++) {
			if (((x - chessboardOffsetX) % squareSize) == 0)
				blackX = !blackX;

			if (blackX)
				pattern_lowres.at<uchar>(y, x) = (uchar)0;
			else
				pattern_lowres.at<uchar>(y, x) = (uchar)255;
		}
	}

	namedWindow("Chessboard", CV_WINDOW_NORMAL);
	cv::moveWindow("Chessboard", 3444, 0);
	cv::setWindowProperty("Chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	imshow("Chessboard", pattern_lowres);
}

void storeIntrinsicsAndPoints() {
	// Store world and model points
	cout << "Storing intrinsics..." << endl;
	FileStorage fs(kinectProjectorIntrinsics, FileStorage::WRITE);
	fs << "camera_matrix" << cameraMatrixProj;
	fs << "distortion_coefficients" << distCoeffsProj;
	fs.release();

	cout << "Storing points..." << endl;
	FileStorage fsp("world_and_model_points.xml", FileStorage::WRITE);

	fsp << "cornerWorldPoints" << cornerWorldPoints;
	fsp << "cornerPatternPoints" << cornerPatternPoints;
	fsp.release();

	cout << "Done." << endl;
}


static bool runProjectorCalibration(Mat& projCameraMatrix, Mat& projDistCoeffs, vector<Mat>& rvecs, vector<Mat>& tvecs)
{

	cout << "Running calibration...";

	//cameraMatrix = Mat::eye(3, 3, CV_64F);

	projCameraMatrix = (Mat1d(3, 3) <<
		PROJ_WIDTH_LOW_RES, 0, PROJ_WIDTH_LOW_RES / 2.,
		0, PROJ_HEIGHT_LOW_RES, PROJ_HEIGHT_LOW_RES / 2.,
		0, 0, 1);
	projDistCoeffs = Mat::zeros(8, 1, CV_64F);
 
	//distCoeffs = distCoeffsProj;

	int flags = CV_CALIB_USE_INTRINSIC_GUESS;//CALIB_FIX_K1 | CALIB_FIX_K2 | CALIB_FIX_K3;
	//flags |= CALIB_FIX_ASPECT_RATIO;
	//flags |= CALIB_FIX_PRINCIPAL_POINT;
	//flags |= CALIB_ZERO_TANGENT_DIST;
	//flags |= CALIB_USE_INTRINSIC_GUESS;

	// Fix aspect ratio
	//cameraMatrix.at<double>(0, 0) = 1;

	//Find intrinsic camera parameters

	Size imageSize = Size(PROJ_WIDTH_LOW_RES, PROJ_HEIGHT_LOW_RES);
	double rms;
	rms = calibrateCamera(cornerWorldPoints, cornerPatternPoints, imageSize, projCameraMatrix, projDistCoeffs, rvecs, tvecs, flags);

	cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

	bool ok = checkRange(projCameraMatrix) && checkRange(projDistCoeffs);

	if (ok) {
		cout << "CALIBRATED!";
		cout << "cameraMatrix = " << endl << " " << projCameraMatrix << endl << endl;
		cout << "tvecs[0] =" << tvecs[0] << endl;

		storeIntrinsicsAndPoints();
	}
	else 
		cout << "CALIBRATION ERROR!" << endl;	

	return ok;
}




Point3d findWorldPointforRGB(Point2i rgbPoint) {
	/* Find nearest depth mapped rgb pixel with KD radiusSearch*/
	flann::KDTreeIndexParams indexParams;

	flann::Index kdtree(Mat(rgbPointsMapped).reshape(1), indexParams);
	vector<float> query;
	query.push_back(rgbPoint.x);
	query.push_back(rgbPoint.y);

	vector<int> indices;
	vector<float> dists;

	int radius = 6;
	int maxValues = 1;
	kdtree.radiusSearch(query, indices, dists, radius, maxValues);

	int foundIndex = indices[0];

	Point3d depthWorld = depthWorldCoords.at(foundIndex);

	return depthWorld;
}

void alignImages(Mat& pRGB, Mat& pDepth, Mat& finalcolorpic) {
	printf("insi-de align images");
	//Mat a = rgbir.at(0);
	//cout << a << endl;
//	Sleep(2000);
	
    int height = 424;
    int width = 512;

	cv::Mat_<double> pt(3, 1);
    
	cv::Mat_<float> cameraIRMat(3, 3), cameraRGBMat(3, 3), fundamentalMat(3, 3);
    
    for (int row = 0; row<height; ++row)
    {
        for (int col = 0; col<width; ++col)
        {            
			
			
			float depth = pDepth.at<float>(row, col);
			//cv::imshow("IR", rgbir.at(1));
			//printf("depth rows -->%d depth cols -->%d", pDepth.rows, pDepth.cols);
			//if (depth > 0.1) {

				//Sleep(10000);
				// Map depthcam depth to 3D point
				pt(0) = depth*(row - cx_depthcam) / fx_depthcam;  // No need for a 3D point buffer
				
				pt(1) = depth*(col - cy_depthcam) / fy_depthcam;  // here, unless you need one.				
				pt(2) = depth;
								
				
				Point3d depthWorldPoint;
				
				depthWorldPoint.x = pt(0);
				depthWorldPoint.y = pt(1);
				depthWorldPoint.z = pt(2);
				depthWorldCoords.push_back(depthWorldPoint);
				
				pt=rotationMat.inv()*pt-translationMat;
				//pt = rotationMat*pt + translationMat;
				
				// Project 3D point to rgbcam
				float x_rgbcam = fx_rgbcam*pt(0) / pt(2) + cx_rgbcam;
				float y_rgbcam = fy_rgbcam*pt(1) / pt(2) + cy_rgbcam;

				//printf("Value of x_rgbcam %f", x_rgbcam);
				//printf("Value of y_rgbcam %f", y_rgbcam);

				int px_rgbcam = 0;
				int py_rgbcam = 0;
				
				Point2i rgbcoords;
				if(x_rgbcam >=0 && y_rgbcam >=0 &&  x_rgbcam < 640 && y_rgbcam < 360){
					// "Interpolate" pixel coordinates (Nearest Neighbors, as discussed above)
					px_rgbcam = cvRound(x_rgbcam);
					py_rgbcam = cvRound(y_rgbcam);
					//printf("valx%dvaly%d", px_rgbcam, py_rgbcam);
					
					rgbcoords.x = px_rgbcam;
					rgbcoords.y = py_rgbcam;
				}
				
				rgbPointsMapped.push_back(rgbcoords);
				Vec4b temp;
				temp = pRGB.at<Vec4b>(py_rgbcam, px_rgbcam);
				finalcolorpic.at<Vec4b>(py_rgbcam, px_rgbcam) = temp;
                
				//finalcolorpic(px_rgbcam,py_rgbcam) = pRgb.at<uchar>(px_rgbcam, py_rgbcam);
				//printf("px_rgbcam: %d py_rgbcam %ld\n", px_rgbcam, py_rgbcam);
				//printf("val: %d \n", pRGB.at<uchar>(row, col));
			       
        }
		
    }
}


void createMapPoints(Mat &rgb, Mat &d, vector<Point2f> &patternPoints) {
    vector<Point2f> rgbcorners;
	//vector<Point2f> circleBuf;

	cout << "Taking sample...\n";
	bool found = findChessboardCorners(rgb, patternSize, rgbcorners);
	if (found) {
		rgbCornerPoints.push_back(rgbcorners);
        Mat viewGrayRGB;
		cvtColor(rgb, viewGrayRGB, CV_BGRA2GRAY);
		cornerSubPix(viewGrayRGB, rgbcorners, Size(11, 11),
			Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
            
        vector<Point3f> foundWorldPoints;
        
        // For each RGB corner point find according world point
		for (int i = 0; i < rgbcorners.size(); i++) {
            Point2f rgbcorner = rgbcorners.at(i);
            Point3d rgbworldpoint = findWorldPointforRGB(rgbcorner);
            foundWorldPoints.push_back(rgbworldpoint);
        }
        
        cornerWorldPoints.push_back(foundWorldPoints);
		cornerPatternPoints.push_back(patternPoints);
    }
}


void on_trackbar_offset(int, void*)
{
	showCalibrationPattern();
}


class UtilityClass
{
public:

	// returns an image pair from the RGB and IR camera
	// Mat.at(0) = RGB; Mat.at(0) = IR; 


};

int main(int argc, char* argv[])
{
	FileStorage fsRGB(RGBparams, FileStorage::READ);
	fsRGB["camera_matrix"] >> cameraMatrixRGB;
	fsRGB["distortion_coefficients"] >> distCoeffsRGB;
	FileStorage fsIR(IRparams, FileStorage::READ);
	fsIR["camera_matrix"] >> cameraMatrixIR;
	fsIR["distortion_coefficients"] >> distCoeffsIR;
	FileStorage  fsStereo(Stereoparams, FileStorage::READ);
	fsStereo["R"] >> rotationMat;
	fsStereo["T"] >> translationMat;

	// rgb intrinsics
	fx_rgbcam = cameraMatrixRGB.at<double>(0, 0);
	fy_rgbcam = cameraMatrixRGB.at<double>(1, 1);
	cx_rgbcam = cameraMatrixRGB.at<double>(0, 2);
	cy_rgbcam = cameraMatrixRGB.at<double>(1, 2);

	//depth intrinsics
	fx_depthcam = cameraMatrixIR.at<double>(0, 0);
	fy_depthcam = cameraMatrixIR.at<double>(1, 1);
	cx_depthcam = cameraMatrixIR.at<double>(0, 2);
	cy_depthcam = cameraMatrixIR.at<double>(1, 2);
    

	if (FAILED(camconverter.kinectSensorIntialise())) {

		std::cout << "No KINECT device found. Aborting" << std::endl;
		Sleep(2000);
		exit(-1);
	}
    
	/*
	createTrackbar("CB Size", "RGB-D", &squareSize, squareSizeMax, on_trackbar_offset);
	createTrackbar("X-Offset", "RGB-D", &chessboardOffsetX, chessboardOffsetMaxX, on_trackbar_offset);
	createTrackbar("Y-Offset", "RGB-D", &chessboardOffsetY, chessboardOffsetMaxY, on_trackbar_offset);
	createTrackbar("BackgroundColor", "RGB-D", &chessboardBGColor, 255, on_trackbar_offset);

	showCalibrationPattern();
	*/
	const char ESC_KEY = 27;
    for (;;)
	{
		
        vector<Mat> views;
        views = camconverter.nextStereoImagePair();
	
		//cout << rgbmat;
		imshow("RGB", views.at(0));
		imshow("IR", views.at(1));
		
		Mat rgbmat = views.at(0);
		Mat depthmat = views.at(1);
        Mat undistortir;
		Mat undistortrgb;
        undistort(depthmat, undistortir, cameraMatrixIR, distCoeffsIR);
		undistort(rgbmat, undistortrgb, cameraMatrixRGB, distCoeffsRGB);
		//views.at(0) = undistortir;
		alignImages(undistortrgb, depthmat, finalColorpic);
		//alignImages(undistortrgb, undistortir, finalColorpic);
        //alignImages(rgbmat, undistortir, finalColorpic);
		//oldAlignImages( finalColorpic, views);
		
		
		imshow("ALIGNED", finalColorpic);
		char key;
		key = (char)waitKey(50);
		

		if (key == ESC_KEY)
			break;
        /*
        vector<Point2f> patternPoints;
		bool found = findChessboardCorners(pattern_lowres, patternSize, patternPoints);
		if (cornerWorldPoints.size() < 2) {
			createMapPoints(rgbmat, undistortir, patternPoints);
		}
		else {
			vector<Mat> R_vec, T_vec;
			calibrated = runProjectorCalibration(cameraMatrixProj, distCoeffsProj, R_vec, T_vec);

		}*/
    }
    
}