#include "cameraconverter.cpp"

/*
Chage the boolean value USE_PROJECTOR_INTR to true to use projector intrinsics
Chage the boolean value USE_PROJECTOR_EXTR to true to use projector extrinsics
Change the value of DO_PROJECTION, USE_PROJECTOR_EXTR,USE_PROJECTOR_INTR to true to do projection of RGB point
For initial startup keep all these 3 parmeters to false
 */

#ifdef __WIN32__
#include <Windows.h>
#endif
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>



#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;

const int IR_ROWS = 424;
const int IR_COLUMNS = 512;

const int RGB_COLUMNS =  640;
const int RGB_ROWS = 360;

vector<Point2f> mappedRgbPoints;

const int MAX_KINECT_VAL = 52685;

const static string RGBparams = "kinectrgb.xml";
const static string IRparams = "kinectir.xml";
const static string Stereoparams = "kinectstereo.xml";

bool calibrated = false;
bool stereoCalibrated = false;


Mat cameraMatrixRGB, distCoeffsRGB;
Mat cameraMatrixIR, distCoeffsIR;
Mat cameraMatrixProj, distCoeffsProj;
Mat rotationMat, translationMat;

Mat rgbdaligned = Mat(IR_ROWS, IR_COLUMNS, CV_8UC4);

vector<Point3d> depthWorldCoords;

vector<vector<Point3f> > cornerWorldPoints;
vector<vector<Point2f> > cornerPatternPoints;
vector<vector<Point2f> > rgbCornerPoints;

double fx_depthcam, fy_depthcam, fx_rgbcam, fy_rgbcam;
double cx_depthcam, cy_depthcam, cx_rgbcam, cy_rgbcam;

const int PROJ_WIDTH_LOW_RES = 1920;
const int PROJ_HEIGHT_LOW_RES = 1080;
const int PROJ_WIDTH_HIGH_RES = 1920;
const int PROJ_HEIGHT_HIGH_RES = 1080;

const bool USE_PROJECTOR_INTR = false;
const bool USE_PROJECTOR_EXTR = false;
const bool DO_PROJECTION = false;

int chessboardOffsetX = 20; // default
int chessboardOffsetY = 20; // default

int squareSize = 100; // default
const int squareSizeMax = 100;

Size patternSize(4, 4);

const int chessboardOffsetMaxX = PROJ_WIDTH_LOW_RES - (patternSize.width + 1)*squareSize;
const int chessboardOffsetMaxY = PROJ_HEIGHT_LOW_RES - (patternSize.height + 1)*squareSize;


int chessboardBGColor = 200;

Mat DepthMatDisp = Mat(424, 512, CV_16UC1);

Mat finalColorpic = Mat(360, 640, CV_8UC4) = { 0 };

Mat pattern_lowres = Mat(PROJ_HEIGHT_LOW_RES, PROJ_WIDTH_LOW_RES, CV_8UC1, chessboardBGColor);
Mat pattern_highres = Mat(PROJ_HEIGHT_HIGH_RES, PROJ_WIDTH_HIGH_RES, CV_8UC1, chessboardBGColor);

const static string kinectProjectorIntrinsics = "projector_intrinsics.xml";
const static string kinectProjectorExtrinsics = "projector_extrinsics.xml";

vector<Point2f> rgbPointsMapped;

Mat kinectRGBRaw = Mat(360, 640, CV_8UC4);
Mat kinectDepthRaw = Mat(424, 512, CV_16UC1);

Mat rvecprojector, tvecprojector;

KinectV2Stream camconverter;

bool isInRange(int val, int max) {
	return val <= max && val >= 0;
}

void rgbWithDepth(Mat& rgb, Mat& ir, Mat& rgbdaligned) { 
	cv::Mat_<double> pt(3, 1);
	rgbPointsMapped.clear();
	depthWorldCoords.clear();
	for (int col = 0; col < 424; col++) {
		for (int row = 0;row < 512;row++) {
			unsigned short depthread = ir.at<ushort>(col, row);
			float depth = static_cast<float>(depthread);
			
			pt(0) = (float)(row - cx_depthcam) * depth / fx_depthcam;
			pt(1) = (float)(col - cy_depthcam) * depth / fy_depthcam;
			pt(2) = (float)depth;

			Point3d depthWorldPoint;
			depthWorldPoint.x = pt(0);
			depthWorldPoint.y = pt(1);
			depthWorldPoint.z = pt(2);
			depthWorldCoords.push_back(depthWorldPoint);

			pt = rotationMat*pt + translationMat;

			float x_rgbcam = fx_rgbcam*pt(0) / pt(2) + cx_rgbcam;
			float y_rgbcam = fy_rgbcam*pt(1) / pt(2) + cy_rgbcam;

			int px_rgbcam = cvRound(x_rgbcam);
			int py_rgbcam = cvRound(y_rgbcam);

			if (px_rgbcam >= 0 && py_rgbcam >= 0 && px_rgbcam < 640 && py_rgbcam < 360) {
				Point2i rgbcoords;
				rgbcoords.x = px_rgbcam;
				rgbcoords.y = py_rgbcam;
				rgbPointsMapped.push_back(rgbcoords);
				rgbdaligned.at<Vec4b>(col,row ) = rgb.at<Vec4b>(py_rgbcam, px_rgbcam);
			}
		}
	}
}

void showCalibrationPattern() {

	pattern_lowres = Mat(PROJ_HEIGHT_LOW_RES, PROJ_WIDTH_LOW_RES, CV_8UC1, chessboardBGColor);

	int maxPatternHeight = chessboardOffsetY + squareSize * (patternSize.height + 1);
	int maxPatternWidth = chessboardOffsetX + squareSize * (patternSize.width + 1);

	if (maxPatternHeight > PROJ_HEIGHT_LOW_RES || maxPatternWidth > PROJ_WIDTH_LOW_RES) {
		cerr << "Chessboard pattern values are out of range!\n";
		return;
	}

	bool blackY = false;
	for (int y = chessboardOffsetY; y < maxPatternHeight; y++) {
		bool blackX = false;
		if (((y - chessboardOffsetY) % squareSize) == 0)
			blackY = !blackY;

		blackX = blackY;

		for (int x = chessboardOffsetX; x < maxPatternWidth; x++) {
			if (((x - chessboardOffsetX) % squareSize) == 0)
				blackX = !blackX;

			if (blackX)
				pattern_lowres.at<uchar>(y, x) = (uchar)0;
			else
				pattern_lowres.at<uchar>(y, x) = (uchar)255;
		}
	}

	namedWindow("Chessboard", CV_WINDOW_NORMAL);
	cv::moveWindow("Chessboard", 1366, 0); 
	cv::setWindowProperty("Chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	imshow("Chessboard", pattern_lowres);
}



void storeProjectorExtrinsics(Mat& rvec, Mat& tvec, Mat& E, Mat& F) {
	// Store world and model points
	cout << "Storing extrinsics..." << endl;
	FileStorage fs(kinectProjectorExtrinsics, FileStorage::WRITE);
	fs << "rvecproj" << rvec;
	fs << "tvecproj" << tvec;
	fs << "essentialproj" << E;
	fs << "fundamentalproj" << F;
	fs.release();

	cout << "Done." << endl;
}

void storeProjectorIntrinsics() {
	// Store world and model points
	cout << "Storing intrinsics..." << endl;
	FileStorage fs(kinectProjectorIntrinsics, FileStorage::WRITE);
	fs << "camera_matrix" << cameraMatrixProj;
	fs << "distortion_coefficients" << distCoeffsProj;
	fs.release();

	cout << "Storing points..." << endl;
	FileStorage fsp("world_and_model_points.xml", FileStorage::WRITE);

	fsp << "cornerWorldPoints" << cornerWorldPoints;
	fsp << "cornerPatternPoints" << cornerPatternPoints;
	fsp.release();

	cout << "Done." << endl;
}

static bool runProjectorStereoCalibration(Mat& rvec, Mat& tvec, Mat& E, Mat& F)
{
	cout << "Running stereo calibration...";
	//Find extrinsic camera parameters
	Size imageSize = Size(PROJ_WIDTH_LOW_RES, PROJ_HEIGHT_LOW_RES);
	double rms;
	rms = stereoCalibrate(cornerWorldPoints, rgbCornerPoints, cornerPatternPoints,
		cameraMatrixRGB, distCoeffsRGB,
		cameraMatrixProj, distCoeffsProj,
		imageSize,
		rvec, tvec, E, F);
	storeProjectorExtrinsics(rvec, tvec, E, F);
	cout << "Re-projection error reported by stereoCalibrateCamera: " << rms << endl;

	cout << "Stereo Tvec:" << tvec << endl;

	return true;
}

static bool runProjectorCalibration(Mat& projCameraMatrix, Mat& projDistCoeffs, vector<Mat>& rvecs, vector<Mat>& tvecs)
{

	cout << "Running calibration...";

	//cameraMatrix = Mat::eye(3, 3, CV_64F);

	projCameraMatrix = (Mat1d(3, 3) <<
		PROJ_WIDTH_LOW_RES, 0, PROJ_WIDTH_LOW_RES / 2.,
		0, PROJ_HEIGHT_LOW_RES, PROJ_HEIGHT_LOW_RES / 2.,
		0, 0, 1);
	projDistCoeffs = Mat::zeros(8, 1, CV_64F);


	int flags = CV_CALIB_USE_INTRINSIC_GUESS;

	Size imageSize = Size(PROJ_WIDTH_LOW_RES, PROJ_HEIGHT_LOW_RES);
	double rms;
	rms = calibrateCamera(cornerWorldPoints, cornerPatternPoints, imageSize, projCameraMatrix, projDistCoeffs, rvecs, tvecs, flags);

	cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

	bool ok = checkRange(projCameraMatrix) && checkRange(projDistCoeffs);

	if (ok) {
		cout << "CALIBRATED!";
		cout << "cameraMatrix = " << endl << " " << projCameraMatrix << endl << endl;
		cout << "tvecs[0] =" << tvecs[0] << endl;

		storeProjectorIntrinsics();
	}
	else
		cout << "CALIBRATION ERROR!" << endl;

	return ok;
}


Point3d findWorldPointforRGB(Point2i rgbPoint) {
	/* Find nearest depth mapped rgb pixel with KD radiusSearch*/
	flann::KDTreeIndexParams indexParams;

	flann::Index kdtree(Mat(rgbPointsMapped).reshape(1), indexParams);
	vector<float> query;
	query.push_back(rgbPoint.x);
	query.push_back(rgbPoint.y);

	vector<int> indices;
	vector<float> dists;

	int radius = 6;
	int maxValues = 1;
	kdtree.radiusSearch(query, indices, dists, radius, maxValues);

	int foundIndex = indices[0];

	Point3d depthWorld = depthWorldCoords.at(foundIndex);

	return depthWorld;
}

void createMapPoints(Mat &rgbmat, Mat &d, vector<Point2f> &patternPoints) {
	vector<Point2f> rgbcorners;
	//vector<Point2f> circleBuf;

	cout << "Inside Create Map Points...\n";
	bool found = findChessboardCorners(rgbmat, patternSize, rgbcorners);
	if (found) {
		cout << "found chess board corners";
		rgbCornerPoints.push_back(rgbcorners);
		Mat viewGrayRGB;
		cvtColor(rgbmat, viewGrayRGB, CV_BGRA2GRAY);
		cornerSubPix(viewGrayRGB, rgbcorners, Size(11, 11),
			Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));

		vector<Point3f> foundWorldPoints;

		// RGB corner point to world point
		for (int i = 0; i < rgbcorners.size(); i++) {
			Point2f rgbcorner = rgbcorners.at(i);
			Point3d rgbworldpoint = findWorldPointforRGB(rgbcorner);
			foundWorldPoints.push_back(rgbworldpoint);
		}

		cornerWorldPoints.push_back(foundWorldPoints);
		cornerPatternPoints.push_back(patternPoints);
	}
}


void on_trackbar_offset(int, void*)
{
	showCalibrationPattern();
}

boolean firstClick = true;

static void onRGBImageClick(int event, int x, int y, int, void*)
{
	if (event != EVENT_LBUTTONDOWN)
		return;

	cout << "Clicked on RGB: ";

	if (!USE_PROJECTOR_INTR) {
		cout << "Can not test projecting. Not calibrated yet..." << endl;
		return;
	}

	cout << "Selected pixel " << x << " : " << y << " pixel\n";

	Point3d worldPoint = findWorldPointforRGB(Point2i(x, y));

	cout << "Clicked on world " << worldPoint.x << " " << worldPoint.y << " " << worldPoint.z << "!\n";

	
	vector<Point3f> points;
	points.push_back(worldPoint);
	Mat rvec = Mat::zeros(3, 1, CV_64F);
	Mat tvec = Mat::zeros(3, 1, CV_64F);

	Mat projImagePoint;
	//projectPoints(points, rvecprojector, tvecprojector, cameraMatrixProj, distCoeffsProj, projImagePoint);
	projectPoints(points, rvec, tvec, cameraMatrixProj, distCoeffsProj, projImagePoint);
	// mirror
	int proj_x = PROJ_WIDTH_LOW_RES - projImagePoint.at<float>(0, 0);

	//int proj_x = projImagePoint.at<float>(0, 0);
	int proj_y = projImagePoint.at<float>(0, 1);

	cout << "Projected image point loc--> " << proj_x << " " << proj_y << endl;

	// Creates calibration pattern if not existent yet
	/*
	if (firstClick)
		showCalibrationPattern();*/

	Scalar color = 155;

	circle(pattern_lowres, Point2d(proj_x, proj_y), 5, Scalar(0, 0, 155), -1, 0, 0);

	imshow("Chessboard", pattern_lowres);

	firstClick = false;
}


int main(int argc, char* argv[])
{
	FileStorage fsRGB(RGBparams, FileStorage::READ);
	fsRGB["camera_matrix"] >> cameraMatrixRGB;
	fsRGB["distortion_coefficients"] >> distCoeffsRGB;
	FileStorage fsIR(IRparams, FileStorage::READ);
	fsIR["camera_matrix"] >> cameraMatrixIR;
	fsIR["distortion_coefficients"] >> distCoeffsIR;
	FileStorage  fsStereo(Stereoparams, FileStorage::READ);
	fsStereo["R"] >> rotationMat;
	fsStereo["T"] >> translationMat;

	// rgb intrinsics
	fx_rgbcam = cameraMatrixRGB.at<double>(0, 0);
	fy_rgbcam = cameraMatrixRGB.at<double>(1, 1);
	cx_rgbcam = cameraMatrixRGB.at<double>(0, 2);
	cy_rgbcam = cameraMatrixRGB.at<double>(1, 2);

	//depth intrinsics
	fx_depthcam = cameraMatrixIR.at<double>(0, 0);
	fy_depthcam = cameraMatrixIR.at<double>(1, 1);
	cx_depthcam = cameraMatrixIR.at<double>(0, 2);
	cy_depthcam = cameraMatrixIR.at<double>(1, 2);

	if (USE_PROJECTOR_INTR) {
		// Load from previous calibration
		FileStorage fsPr(kinectProjectorIntrinsics, FileStorage::READ);
		fsPr["camera_matrix"] >> cameraMatrixProj;
		fsPr["distortion_coefficients"] >> distCoeffsProj;
	}

	if (USE_PROJECTOR_EXTR) {
		// Load from previous calibration
		FileStorage fsPr(kinectProjectorExtrinsics, FileStorage::READ);
		fsPr["rvecproj"] >> rvecprojector;
		fsPr["tvecproj"] >> tvecprojector;
	}

	namedWindow("RGB");
	//namedWindow("ALIGNED");
	setMouseCallback("RGB", onRGBImageClick, 0);

	if (FAILED(camconverter.kinectSensorIntialise())) {

		std::cout << "No KINECT device found. Aborting" << std::endl;
		Sleep(2000);
		exit(-1);
	}


	

	for(;;)
	{
		vector<Mat> views = camconverter.nextStereoImagePair();
		Mat rgbmat = views.at(0);
		Mat d = views.at(1);

		Mat tempRGB, undistortir;
		//tempRGB = rgb.clone();
		//tempD = d.clone();

		// undistort with intrinsics
		undistort(rgbmat, tempRGB, cameraMatrixRGB, distCoeffsRGB);
		undistort(d, undistortir, cameraMatrixIR, distCoeffsIR);

		// mirror horizontal
		//cv::flip(tempRGB, rgb, 1);
		//cv::flip(d, tempD, 1);

		rgbmat = tempRGB;
		d = undistortir;
		//d = tempD;
		rgbWithDepth(rgbmat, d, rgbdaligned);
		
		char key = (char)waitKey(50);
		if (!DO_PROJECTION) {

			createTrackbar("CB Size", "ALIGNED", &squareSize, squareSizeMax, on_trackbar_offset);
			createTrackbar("X-Offset", "ALIGNED", &chessboardOffsetX, chessboardOffsetMaxX, on_trackbar_offset);
			createTrackbar("Y-Offset", "ALIGNED", &chessboardOffsetY, chessboardOffsetMaxY, on_trackbar_offset);
			createTrackbar("BackgroundColor", "ALIGNED", &chessboardBGColor, 255, on_trackbar_offset);

			showCalibrationPattern();
			if (key == 'c') {

				vector<Point2f> patternPoints;
				bool found = findChessboardCorners(pattern_lowres, patternSize, patternPoints);
				if (!found) {
					cerr << "Could not detect chessboard corners in model!";
				}
				// Debug preview of found chessboard corners
				drawChessboardCorners(pattern_lowres, patternSize, Mat(patternPoints), found);
				imshow("Chessboard", pattern_lowres);
				waitKey(50);

				if (cornerWorldPoints.size() < 4) {
					createMapPoints(rgbmat, undistortir, patternPoints);

				}
				else {
					vector<Mat> R_vec, T_vec;
					calibrated = runProjectorCalibration(cameraMatrixProj, distCoeffsProj, R_vec, T_vec);
					Mat rvec, tvec, E, F;
					stereoCalibrated = runProjectorStereoCalibration(rvec, tvec, E, F);
				}
			}
			imshow("ALIGN", rgbdaligned);
		}
		imshow("RGB", rgbmat);
		

		if (waitKey(50) == 27) break; // wait for esc
	}
}