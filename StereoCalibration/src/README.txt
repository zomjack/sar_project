Code from the OpenCV camera calibration tutorial:
http://docs.opencv.org/doc/tutorials/calib3d/camera_calibration/camera_calibration.html

Instructions
After a successful build:
- copy config.xml, imagelist.xml and camera images into your build directory (i.e. where the calibcv executable is)
- for test purposes run ./calibcv config.xml 
  This will open the test images and determine the camera intrinsic, extrinsic and distortion parameters.
- refer to the comments in config.xml for using a live camera

For use with a KinectV2, the Kinect for Windows SDK 2 is needed.
The RGB camera with FullHD resolution can be calibrated by enabling 
<Input>"kinectv2_rgb"</Input>
The RGB camera with 640x360 resolution can be calibrated by enabling 
<Input>"kinectv2_rgb_640"</Input>
The infrared camera (used for generating depth images)  can be calibrated by enabling 
<Input>"kinectv2_ir"</Input>