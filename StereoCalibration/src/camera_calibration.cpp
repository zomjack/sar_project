#include "kinectv2stream.cpp"

#ifdef __WIN32__
#include <Windows.h>
#endif
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>



#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;


Mat kinectRGBImageRaw = Mat(1080, 1920, CV_8UC4);
Mat kinectRGBImageResized = Mat(360, 640, CV_8UC4);
Mat DepthMatRaw = Mat(424, 512, CV_16UC1);
Mat DepthMatDisp = Mat(424, 512, CV_16UC1);
Mat InfraredRawMat = Mat(424, 512, CV_16UC1);
Mat InfraredNormalisedMat8bit = Mat(424, 512, CV_8UC3);
Mat InfraredNormalisedMat = Mat(424, 512, CV_16UC1);


int MAX_DEPTH = 8000; // max sensing range of kinect is 8000 mm 
int MAX_USHORT_VALUE = 65535;
// maxVal = 52685

ushort DEPTH_DISPLAY_SCALE = (ushort)(1.0 / MAX_DEPTH * MAX_USHORT_VALUE);



KinectV2Stream ks;

#define IR_ROWS 424
#define IR_COLUMNS 512

// for displaying the rectified stereo images
Mat rectifiedCanvas;
Mat rmapRGB[2];
Mat rmapIR[2];
Rect validRoi[2];
double sf;

static void help()
{
    cout <<  "This is a camera calibration sample." << endl
         <<  "Usage: calibration configurationFile"  << endl
         <<  "Near the sample file you'll find the configuration file, which has detailed help of "
             "how to edit it.  It may be any OpenCV supported file format XML/YAML." << endl;
}
class Settings
{
public:
    Settings() : goodInput(false) {}
    enum Pattern { NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID };
	enum InputType { INVALID, CAMERA, VIDEO_FILE, IMAGE_LIST, KINECT_RGB, KINECT_RGB_640, KINECT_IR, KINECT_STEREO, KINECT_STEREO_640 };

    void write(FileStorage& fs) const                        //Write serialization for this class
    {
        fs << "{"
                  << "BoardSize_Width"  << boardSize.width
                  << "BoardSize_Height" << boardSize.height
                  << "Square_Size"         << squareSize
                  << "Calibrate_Pattern" << patternToUse
                  << "Calibrate_NrOfFrameToUse" << nrFrames
                  << "Calibrate_FixAspectRatio" << aspectRatio
                  << "Calibrate_AssumeZeroTangentialDistortion" << calibZeroTangentDist
                  << "Calibrate_FixPrincipalPointAtTheCenter" << calibFixPrincipalPoint

                  << "Write_DetectedFeaturePoints" << writePoints
                  << "Write_extrinsicParameters"   << writeExtrinsics
                  << "Write_outputFileName"  << outputFileName

                  << "Show_UndistortedImage" << showUndistorsed

                  << "Input_FlipAroundHorizontalAxis" << flipVertical
                  << "Input_Delay" << delay
                  << "Input" << input
           << "}";
    }
    void read(const FileNode& node)                          //Read serialization for this class
    {
        node["BoardSize_Width" ] >> boardSize.width;
        node["BoardSize_Height"] >> boardSize.height;
        node["Calibrate_Pattern"] >> patternToUse;
        node["Square_Size"]  >> squareSize;
        node["Calibrate_NrOfFrameToUse"] >> nrFrames;
        node["Calibrate_FixAspectRatio"] >> aspectRatio;
        node["Write_DetectedFeaturePoints"] >> writePoints;
        node["Write_extrinsicParameters"] >> writeExtrinsics;
        node["Write_outputFileName"] >> outputFileName;
        node["Calibrate_AssumeZeroTangentialDistortion"] >> calibZeroTangentDist;
        node["Calibrate_FixPrincipalPointAtTheCenter"] >> calibFixPrincipalPoint;
        node["Calibrate_UseFisheyeModel"] >> useFisheye;
        node["Input_FlipAroundHorizontalAxis"] >> flipVertical;
        node["Show_UndistortedImage"] >> showUndistorsed;
        node["Input"] >> input;
		node["Input_stereo_use_intrinsic_guess"] >> useIntrinsicGuess;
		node["Input_rgb_calib"] >> kinectRGBIntrinsics;
		node["Input_ir_calib"] >> kinectIRIntrinsics;
        node["Input_Delay"] >> delay;

        validate();
    }
    
    
    void validate()
    {
        goodInput = true;
        if (boardSize.width <= 0 || boardSize.height <= 0)
        {
            cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
            goodInput = false;
        }
        if (squareSize <= 10e-6)
        {
            cerr << "Invalid square size " << squareSize << endl;
            goodInput = false;
        }
        if (nrFrames <= 0)
        {
            cerr << "Invalid number of frames " << nrFrames << endl;
            goodInput = false;
        }

        if (input.empty())      // Check for valid input
                inputType = INVALID;
        else
        {
            if (input[0] >= '0' && input[0] <= '9')
            {
                stringstream ss(input);
                ss >> cameraID;
                inputType = CAMERA;
			}
			else if (input.compare("kinectv2_rgb") == 0) // calibrate RGB camera only
			{
				inputType = KINECT_RGB;
			}
			else if (input.compare("kinectv2_rgb_640") == 0) // calibrate RGB camera only
			{
				inputType = KINECT_RGB_640;
			}
			else if (input.compare("kinectv2_ir") == 0) // calibrate IR camera only
			{
				inputType = KINECT_IR;
			}
			else if (input.compare("kinectv2_stereo") == 0) // stereo calibrate RGB-IR cameras
			{
				inputType = KINECT_STEREO;
			}
			else if (input.compare("kinectv2_stereo_640") == 0) // stereo calibrate RGB-IR cameras
			{
				inputType = KINECT_STEREO_640;
			}
            else
            {
                if (readStringList(input, imageList))
                {
                    inputType = IMAGE_LIST;
                    nrFrames = (nrFrames < (int)imageList.size()) ? nrFrames : (int)imageList.size();
                }
                else
                    inputType = VIDEO_FILE;
            }


			if (inputType == KINECT_RGB || inputType == KINECT_RGB_640 || inputType == KINECT_IR || inputType == KINECT_STEREO || inputType == KINECT_STEREO_640) {
				if (FAILED(ks.kinectSensorIntialise())) {
					std::cout << "No KINECT device found. Aborting" << std::endl;
					exit(-1);
				}
			} else   if (inputType == CAMERA) {
                inputCapture.open(cameraID);
                // set resolution
                inputCapture.set(CV_CAP_PROP_FRAME_WIDTH,640);
				inputCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
			}
			else if (inputType == VIDEO_FILE) {
				inputCapture.open(input);
			}
			else {
				if (inputType != IMAGE_LIST && !inputCapture.isOpened())
					inputType = INVALID;
			}
        }
        if (inputType == INVALID)
        {
            cerr << " Input does not exist: " << input;
            goodInput = false;
        }

        flag = CALIB_FIX_K4 | CALIB_FIX_K5;
        if(calibFixPrincipalPoint) flag |= CALIB_FIX_PRINCIPAL_POINT;
        if(calibZeroTangentDist)   flag |= CALIB_ZERO_TANGENT_DIST;
        if(aspectRatio)            flag |= CALIB_FIX_ASPECT_RATIO;

        if (useFisheye) {
            // the fisheye model has its own enum, so overwrite the flags
            flag = fisheye::CALIB_FIX_SKEW | fisheye::CALIB_RECOMPUTE_EXTRINSIC |
                   // fisheye::CALIB_FIX_K1 |
                   fisheye::CALIB_FIX_K2 | fisheye::CALIB_FIX_K3 | fisheye::CALIB_FIX_K4;
        }

        calibrationPattern = NOT_EXISTING;
        if (!patternToUse.compare("CHESSBOARD")) calibrationPattern = CHESSBOARD;
        if (!patternToUse.compare("CIRCLES_GRID")) calibrationPattern = CIRCLES_GRID;
        if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID")) calibrationPattern = ASYMMETRIC_CIRCLES_GRID;
        if (calibrationPattern == NOT_EXISTING)
        {
            cerr << " Camera calibration mode does not exist: " << patternToUse << endl;
            goodInput = false;
        }
        atImageList = 0;

    }
	// returns an image pair from the RGB and IR camera
	// Mat.at(0) = RGB; Mat.at(0) = IR; 
	vector<Mat> nextStereoImagePair() {
		Mat rgb;
		Mat ir;
		vector<Mat> rgbIR = vector<Mat>();
		
		if (inputType == KINECT_STEREO) {
			ks.getRGBFeed(kinectRGBImageRaw);
			rgbIR.push_back(kinectRGBImageRaw);
		}
		else if (inputType == KINECT_STEREO_640) {
			ks.getRGBFeedOptimised(kinectRGBImageResized);
			rgbIR.push_back(kinectRGBImageResized);
		}
		
		ks.getInfraredFrameNormalized8bit(InfraredNormalisedMat8bit);
		rgbIR.push_back(InfraredNormalisedMat8bit);

		return rgbIR;		
	}
	
    Mat nextImage()
    {
        Mat result;

		if (inputType == KINECT_RGB) {
			ks.getRGBFeed(kinectRGBImageRaw);
			kinectRGBImageRaw.copyTo(result);			
		}
		else if (inputType == KINECT_RGB_640) {

			ks.getRGBFeedOptimised(kinectRGBImageResized);
			kinectRGBImageResized.copyTo(result);
			
		}
		else if (inputType == KINECT_IR) {
			//ks.getRGBFeedOptimised(kinectRGBImageRaw);
			//ks.getInfraredFrameNormalized(InfraredNormalisedMat);
			ks.getInfraredFrameNormalized8bit(InfraredNormalisedMat8bit);

			
			//InfraredRawMat8bit = ((1.0 / (float)MAX_USHORT_VALUE * 255.0) * InfraredNormalisedMat);
			//InfraredNormalisedMat8bit = ((1.0 / (float)USHRT_MAX * 255.0) * InfraredNormalisedMat);

			//InfraredNormalisedMat.copyTo(result);
			InfraredNormalisedMat8bit.copyTo(result);
		}
		else if( inputCapture.isOpened() )
        {
            Mat view0;
            inputCapture >> view0;
            view0.copyTo(result);
        }
        else if( atImageList < imageList.size() )
            result = imread(imageList[atImageList++], IMREAD_COLOR);

        return result;
    }

    static bool readStringList( const string& filename, vector<string>& l )
    {
        l.clear();
        FileStorage fs(filename, FileStorage::READ);
        if( !fs.isOpened() )
            return false;
        FileNode n = fs.getFirstTopLevelNode();
        if( n.type() != FileNode::SEQ )
            return false;
        FileNodeIterator it = n.begin(), it_end = n.end();
        for( ; it != it_end; ++it )
            l.push_back((string)*it);
        return true;
    }
public:
    Size boardSize;              // The size of the board -> Number of items by width and height
    Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
    float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
    int nrFrames;                // The number of frames to use from the input for calibration
    float aspectRatio;           // The aspect ratio
    int delay;                   // In case of a video input
    bool writePoints;            // Write detected feature points
    bool writeExtrinsics;        // Write extrinsic parameters
    bool calibZeroTangentDist;   // Assume zero tangential distortion
    bool calibFixPrincipalPoint; // Fix the principal point at the center
    bool flipVertical;           // Flip the captured images around the horizontal axis
    string outputFileName;       // The name of the file where to write
    bool showUndistorsed;        // Show undistorted images after calibration
    string input;                // The input ->
    bool useFisheye;             // use fisheye camera model for calibration

	// for stereo calibration
	bool useIntrinsicGuess;
	string kinectRGBIntrinsics;
	string kinectIRIntrinsics;

    int cameraID;
    vector<string> imageList;
    size_t atImageList;
    VideoCapture inputCapture;
    InputType inputType;
    bool goodInput;
    int flag;

private:
    string patternToUse;


};

void stereoCalibrate(Settings s);
void cameraCalibrate(Settings s);

static inline void read(const FileNode& node, Settings& x, const Settings& default_value = Settings())
{
    if(node.empty())
        x = default_value;
    else
        x.read(node);
}

static inline void write(FileStorage& fs, const String&, const Settings& s )
{
    s.write(fs);
}

enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

bool runCalibrationAndSave(Settings& s, Size imageSize, Mat&  cameraMatrix, Mat& distCoeffs,
                           vector<vector<Point2f> > imagePoints );

bool runStereoCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrixRGB, Mat& distCoeffsRGB,
	vector<vector<Point2f> > imagePointsRGB, Mat& cameraMatrixIR, Mat& distCoeffsIR,
	vector<vector<Point2f> > imagePointsIR, bool useIntrinsicGuess);

int main(int argc, char* argv[])
{
	help();

	//! [file_read]
	Settings s;
	const string inputSettingsFile = argc > 1 ? argv[1] : "default.xml";
	FileStorage fs(inputSettingsFile, FileStorage::READ); // Read the settings
	if (!fs.isOpened())
	{
		cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
		return -1;
	}
	fs["Settings"] >> s;
	fs.release();                                         // close Settings file
	//! [file_read]

	//FileStorage fout("settings.yml", FileStorage::WRITE); // write config as YAML
	//fout << "Settings" << s;

	if (!s.goodInput)
	{
		cout << "Invalid input detected. Application stopping. " << endl;
		return -1;
	}

	if (s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) {
		stereoCalibrate(s);
	}
	else {
		cameraCalibrate(s);
	}
}

void stereoCalibrate(Settings s) {
	vector<vector<Point2f> > imagePointsRGB;
	vector<vector<Point2f> > imagePointsIR;
	Mat cameraMatrixRGB, distCoeffsRGB;
	Mat cameraMatrixIR, distCoeffsIR;

	if (s.useIntrinsicGuess == true) {
		FileStorage fsRGB(s.kinectRGBIntrinsics, FileStorage::READ);
		fsRGB["camera_matrix"] >> cameraMatrixRGB;
		fsRGB["distortion_coefficients"] >> distCoeffsRGB;
		FileStorage fsIR(s.kinectIRIntrinsics, FileStorage::READ);
		fsIR["camera_matrix"] >> cameraMatrixIR;
		fsIR["distortion_coefficients"] >> distCoeffsIR;

		cout << "RGB intrinsics: " << cameraMatrixRGB << endl;
		cout << "RGB distortion coefficients: " << distCoeffsRGB << endl;

		cout << "IR intrinsics: " << cameraMatrixIR << endl;
		cout << "IR distortion coefficients: " << distCoeffsIR << endl;
	}
	Size imageSize, imageSizeRGB, imageSizeIR;
	int mode = s.inputType == DETECTION;

	clock_t prevTimestamp = 0;
	const Scalar RED(0, 0, 255), GREEN(0, 255, 0);
	const char ESC_KEY = 27;
	
	for (;;)
	{
		vector<Mat> views;
		bool blinkOutput = false;
		bool blinked = false;


		views = s.nextStereoImagePair();

		//-----  If no more image, or got enough, then stop calibration and show result -------------
		if (mode == CAPTURING && imagePointsRGB.size() >= (size_t)s.nrFrames)
		{
			if (runStereoCalibrationAndSave(s, imageSize, cameraMatrixRGB, distCoeffsRGB, imagePointsRGB, cameraMatrixIR, distCoeffsIR, imagePointsIR, s.useIntrinsicGuess))
				mode = CALIBRATED;
			else
				mode = DETECTION;
		}
		imageSizeRGB = views.at(0).size(); 
		imageSizeIR = views.at(1).size();
		imageSize = imageSizeRGB;

		if (s.flipVertical) {
			flip(views.at(0), views.at(0), 0);
			flip(views.at(1), views.at(1), 0);
		}

		//! [find_pattern]
		vector<Point2f> pointBufRGB;
		vector<Point2f> pointBufIR;
		bool found, foundRGB, foundIR ;

		int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;

		
		switch (s.calibrationPattern) // Find feature points on the input format
		{
		case Settings::CHESSBOARD:
			foundRGB = findChessboardCorners(views.at(0), s.boardSize, pointBufRGB, chessBoardFlags);
			foundIR = findChessboardCorners(views.at(1), s.boardSize, pointBufIR, chessBoardFlags);
			found = foundRGB && foundIR;
			break;
		case Settings::CIRCLES_GRID:
			foundRGB = findCirclesGrid(views.at(0), s.boardSize, pointBufRGB);
			foundIR = findCirclesGrid(views.at(1), s.boardSize, pointBufIR);
			found = foundRGB && foundIR;
			break;
		case Settings::ASYMMETRIC_CIRCLES_GRID:
			foundRGB = findCirclesGrid(views.at(0), s.boardSize, pointBufRGB, CALIB_CB_ASYMMETRIC_GRID);
			foundIR = findCirclesGrid(views.at(1), s.boardSize, pointBufIR, CALIB_CB_ASYMMETRIC_GRID);
			found = foundRGB && foundIR;
			break;
		default:
			found = false;
			break;
		}
		//! [pattern_found]
		if (found)                // If done with success,
		{
			// improve the found corners' coordinate accuracy for chessboard
			if (s.calibrationPattern == Settings::CHESSBOARD)
			{
				Mat viewGrayRGB, viewGrayIR;
				cvtColor(views.at(0), viewGrayRGB, COLOR_BGR2GRAY);
				cornerSubPix(viewGrayRGB, pointBufRGB, Size(11, 11),
					Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));

				cvtColor(views.at(1), viewGrayIR, COLOR_BGR2GRAY);
				cornerSubPix(viewGrayIR, pointBufIR, Size(11, 11),
					Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
			}

			if (mode == CAPTURING &&  // For camera only take new samples after delay time
				//(!s.inputCapture.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) )
				((s.inputCapture.isOpened() || s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) && clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC))

			{
				imagePointsRGB.push_back(pointBufRGB);
				imagePointsIR.push_back(pointBufIR);
				prevTimestamp = clock();
				
				blinkOutput = true;//!blinkOutput;
			}

			// Draw the corners.
			drawChessboardCorners(views.at(0), s.boardSize, Mat(pointBufRGB), found);
			drawChessboardCorners(views.at(1), s.boardSize, Mat(pointBufIR), found);
		}

		//! [output_text]
		string msg = (mode == CAPTURING) ? "100/100" :
			mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
		int baseLine = 0;
		Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
		Point textOriginRGB(views.at(0).cols - 2 * textSize.width - 10, views.at(0).rows - 2 * baseLine - 10);
		Point textOriginIR(views.at(1).cols - 2 * textSize.width - 10, views.at(1).rows - 2 * baseLine - 10);

		if (mode == CAPTURING)
		{
			if (s.showUndistorsed)
				msg = format("%d/%d Undist", (int)imagePointsRGB.size(), s.nrFrames);
			else
				msg = format("%d/%d", (int)imagePointsRGB.size(), s.nrFrames);
		}

		putText(views.at(0), msg, textOriginRGB, 1, 1, mode == CALIBRATED ? GREEN : RED);
		putText(views.at(1), msg, textOriginIR, 1, 1, mode == CALIBRATED ? GREEN : RED);

		if (blinkOutput) {
			bitwise_not(views.at(0), views.at(0));
			bitwise_not(views.at(1), views.at(1));
			blinkOutput = false;//!blinkOutput;
			blinked = true;
		}
		else if (blinked){
			bitwise_not(views.at(0), views.at(0));
			bitwise_not(views.at(1), views.at(1));
			blinked = false;
		}

		//! [output_text]
		//------------------------- Video capture  output  undistorted ------------------------------
		//! [output_undistorted]
		Mat tempRGB = views.at(0).clone();
		Mat tempIR = views.at(1).clone();
		if (mode == CALIBRATED && s.showUndistorsed)
		{
			
			undistort(tempRGB, views.at(0), cameraMatrixRGB, distCoeffsRGB);
			undistort(tempIR, views.at(1), cameraMatrixIR, distCoeffsIR);
		}

		//// show rectified results for stereo
		//if (mode == CALIBRATED && (s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640))
		//{
		//	Mat rimgRGB;// = views.at(0).clone();
		//	Mat rimgIR;// = views.at(1).clone();

		//	//Mat rimgRGB, cimgRGB;
		//	//Mat rimgIR, cimgIR;
		//	cv::remap(tempRGB, rimgRGB, rmapRGB[0], rmapRGB[1], INTER_LINEAR);
		//	cv::remap(tempIR, rimgIR, rmapIR[0], rmapIR[1], INTER_LINEAR);

		///*	cvtColor(rimgRGB, cimgRGB, COLOR_GRAY2BGR);
		//	cvtColor(rimgIR, cimgIR, COLOR_GRAY2BGR);*/

		//	Mat canvasPartRGB = rectifiedCanvas(Rect(0, 0, rectifiedCanvas.cols / 2, rectifiedCanvas.rows));
		//	Mat canvasPartIR = rectifiedCanvas(Rect(rectifiedCanvas.cols / 2, 0, rectifiedCanvas.cols / 2, rectifiedCanvas.rows));
		//	/*resize(cimgRGB, canvasPartRGB, canvasPartRGB.size(), 0, 0, INTER_AREA);
		//	resize(cimgIR, canvasPartIR, canvasPartIR.size(), 0, 0, INTER_AREA);*/
		//	resize(rimgRGB, canvasPartRGB, canvasPartRGB.size(), 0, 0, INTER_AREA);
		//	resize(rimgIR, canvasPartIR, canvasPartIR.size(), 0, 0, INTER_AREA);
		//	
		//	
		//	Rect vroiRGB(cvRound(validRoi[0].x*sf), cvRound(validRoi[0].y*sf),cvRound(validRoi[0].width*sf), cvRound(validRoi[0].height*sf));
		//	Rect vroiIR(cvRound(validRoi[1].x*sf), cvRound(validRoi[1].y*sf), cvRound(validRoi[1].width*sf), cvRound(validRoi[1].height*sf));
		//	rectangle(canvasPartRGB, vroiRGB, Scalar(0, 0, 255), 3, 8);
		//	rectangle(canvasPartIR, vroiIR, Scalar(0, 0, 255), 3, 8);

		//	for (int j = 0; j < rectifiedCanvas.rows; j += 16) 
		//		line(rectifiedCanvas, Point(0, j), Point(rectifiedCanvas.cols, j), Scalar(0, 255, 0), 1, 8);

		//	//imshow("rectified", rectifiedCanvas);
		//	imshow("rimgRGB", rimgRGB);
		//	imshow("rimgIR", rimgIR);
		//	
		//}

		//! [output_undistorted]
		//------------------------------ Show image and check for input commands -------------------
		//! [await_input]
		cv::imshow("RGB", views.at(0));
		cv::imshow("IR", views.at(1));

		char key;
		if (s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) {
			key = (char)waitKey(50);
		}
		else {
			key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);
		}

		if (key == ESC_KEY)
			break;

		if (key == 'u' && mode == CALIBRATED)
			s.showUndistorsed = !s.showUndistorsed;

		if ((s.inputCapture.isOpened() || s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) && key == 'g')
		{
			mode = CAPTURING;
			imagePointsRGB.clear();
			imagePointsIR.clear();
		}


	} // end for

}
void cameraCalibrate(Settings s) {
	vector<vector<Point2f> > imagePoints;

    Mat cameraMatrix, distCoeffs;
    Size imageSize;
    int mode = s.inputType == Settings::IMAGE_LIST ? CAPTURING : DETECTION;
    clock_t prevTimestamp = 0;
    const Scalar RED(0,0,255), GREEN(0,255,0);
    const char ESC_KEY = 27;
    
    // iterate over all existing cameras
    /*  
    for (int i = 1; i < 1500; i++) {
        if (s.inputCapture.open(i))
        {
            cout << "Found camera " << i << endl;
            //s.inputCapture.release();
            break;
        }
    }*/

	
    //! [get_input]
    for(;;)
    {
        Mat view;
        bool blinkOutput = false;
		bool blinked = false;


        view = s.nextImage();

        //-----  If no more image, or got enough, then stop calibration and show result -------------
        if( mode == CAPTURING && imagePoints.size() >= (size_t)s.nrFrames )
        {
          if( runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints))
              mode = CALIBRATED;
          else
              mode = DETECTION;
        }
        if(view.empty())          // If there are no more images stop the loop
        {
            // if calibration threshold was not reached yet, calibrate now
            if( mode != CALIBRATED && !imagePoints.empty() )
                runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints);
            break;
        }
        //! [get_input]

        imageSize = view.size();  // Format input image.
        if( s.flipVertical )    flip( view, view, 0 );

        //! [find_pattern]
        vector<Point2f> pointBuf;

        bool found;

        int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;

        if(!s.useFisheye) {
            // fast check erroneously fails with high distortions like fisheye
            chessBoardFlags |= CALIB_CB_FAST_CHECK;
        }

        switch( s.calibrationPattern ) // Find feature points on the input format
        {
        case Settings::CHESSBOARD:
            found = findChessboardCorners( view, s.boardSize, pointBuf, chessBoardFlags);
            break;
        case Settings::CIRCLES_GRID:
            found = findCirclesGrid( view, s.boardSize, pointBuf );
            break;
        case Settings::ASYMMETRIC_CIRCLES_GRID:
            found = findCirclesGrid( view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID );
            break;
        default:
            found = false;
            break;
        }
        //! [find_pattern]
        //! [pattern_found]
        if ( found)                // If done with success,
        {
              // improve the found corners' coordinate accuracy for chessboard
                if( s.calibrationPattern == Settings::CHESSBOARD)
                {
                    Mat viewGray;
                    cvtColor(view, viewGray, COLOR_BGR2GRAY);
                    cornerSubPix( viewGray, pointBuf, Size(11,11),
                        Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
                }

                if( mode == CAPTURING &&  // For camera only take new samples after delay time
                    //(!s.inputCapture.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) )
					((s.inputCapture.isOpened() || s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) && clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC))
					
                {
                    imagePoints.push_back(pointBuf);
                    prevTimestamp = clock();
                    //blinkOutput = s.inputCapture.isOpened();
					blinkOutput = true;//!blinkOutput;
                }

                // Draw the corners.
                drawChessboardCorners( view, s.boardSize, Mat(pointBuf), found );
        }
        //! [pattern_found]
        //----------------------------- Output Text ------------------------------------------------
        //! [output_text]
        string msg = (mode == CAPTURING) ? "100/100" :
                      mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
        int baseLine = 0;
        Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
        Point textOrigin(view.cols - 2*textSize.width - 10, view.rows - 2*baseLine - 10);

        if( mode == CAPTURING )
        {
            if(s.showUndistorsed)
                msg = format( "%d/%d Undist", (int)imagePoints.size(), s.nrFrames );
            else
                msg = format( "%d/%d", (int)imagePoints.size(), s.nrFrames );
        }

        putText( view, msg, textOrigin, 1, 1, mode == CALIBRATED ?  GREEN : RED);

		if (blinkOutput) {
			bitwise_not(view, view);
			blinkOutput = false;//!blinkOutput;
			blinked = true;
		}
		else if (blinked){
			bitwise_not(view, view);
			blinked = false;
		}


        //! [output_text]
        //------------------------- Video capture  output  undistorted ------------------------------
        //! [output_undistorted]
        if( mode == CALIBRATED && s.showUndistorsed )
        {
            Mat temp = view.clone();
            undistort(temp, view, cameraMatrix, distCoeffs);
        }
        //! [output_undistorted]
        //------------------------------ Show image and check for input commands -------------------
        //! [await_input]
        cv::imshow("Image View", view);

		char key;
		if (s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) {
			key = (char)waitKey(50);
		}
		else {
			key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);
		}

        if( key  == ESC_KEY )
            break;

        if( key == 'u' && mode == CALIBRATED )
           s.showUndistorsed = !s.showUndistorsed;

		if ((s.inputCapture.isOpened() || s.inputType == s.KINECT_RGB || s.inputType == s.KINECT_RGB_640 || s.inputType == s.KINECT_IR || s.inputType == s.KINECT_STEREO || s.inputType == s.KINECT_STEREO_640) && key == 'g')
        {
            mode = CAPTURING;
            imagePoints.clear();
        }
        //! [await_input]
    }

    // -----------------------Show the undistorted image for the image list ------------------------
    //! [show_results]
    if( s.inputType == Settings::IMAGE_LIST && s.showUndistorsed )
    {
        Mat view, rview, map1, map2;

        if (s.useFisheye)
        {
            Mat newCamMat;
            fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imageSize,
                                                                Matx33d::eye(), newCamMat, 1);
            fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Matx33d::eye(), newCamMat, imageSize,
                                             CV_16SC2, map1, map2);
        }
        else
        {
            initUndistortRectifyMap(
                cameraMatrix, distCoeffs, Mat(),
                getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize,
                CV_16SC2, map1, map2);
        }

        for(size_t i = 0; i < s.imageList.size(); i++ )
        {
            view = imread(s.imageList[i], 1);
            if(view.empty())
                continue;
            cv::remap(view, rview, map1, map2, INTER_LINEAR);
            cv::imshow("Image View", rview);
            char c = (char)waitKey();
            if( c  == ESC_KEY || c == 'q' || c == 'Q' )
                break;
        }
    }
    //! [show_results]

    //return 0;
}

//! [compute_errors]
static double computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
                                         const vector<vector<Point2f> >& imagePoints,
                                         const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                                         const Mat& cameraMatrix , const Mat& distCoeffs,
                                         vector<float>& perViewErrors, bool fisheye)
{
    vector<Point2f> imagePoints2;
    size_t totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for(size_t i = 0; i < objectPoints.size(); ++i )
    {
        if (fisheye)
        {
            fisheye::projectPoints(objectPoints[i], imagePoints2, rvecs[i], tvecs[i], cameraMatrix,
                                   distCoeffs);
        }
        else
        {
            projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
        }
        err = norm(imagePoints[i], imagePoints2, NORM_L2);

        size_t n = objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr        += err*err;
        totalPoints     += n;
    }

    return std::sqrt(totalErr/totalPoints);
}
//! [compute_errors]
//! [board_corners]
static void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
                                     Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
    corners.clear();

    switch(patternType)
    {
    case Settings::CHESSBOARD:
    case Settings::CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; ++i )
            for( int j = 0; j < boardSize.width; ++j )
                corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
        break;

    case Settings::ASYMMETRIC_CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; i++ )
            for( int j = 0; j < boardSize.width; j++ )
                corners.push_back(Point3f((2*j + i % 2)*squareSize, i*squareSize, 0));
        break;
    default:
        break;
    }
}
static bool runStereoCalibration(Settings& s, Size& imageSize, Mat& cameraMatrixRGB, Mat& distCoeffsRGB,
	vector<vector<Point2f> > imagePointsRGB, Mat& cameraMatrixIR, Mat& distCoeffsIR,
	vector<vector<Point2f> > imagePointsIR, Mat& R, Mat& T,
	Mat& essentialMat, Mat& fundamentalMat,
	double& rmsErr, double& avgEpipolarErr, bool useIntrinsicGuess)
{

	vector<vector<Point3f> > objectPoints(1);
	calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

	objectPoints.resize(imagePointsRGB.size(), objectPoints[0]);


	if (useIntrinsicGuess) {
		rmsErr = stereoCalibrate(objectPoints, imagePointsRGB, imagePointsIR,
			cameraMatrixRGB, distCoeffsRGB,
			cameraMatrixIR, distCoeffsIR,
			imageSize, R, T, essentialMat, fundamentalMat,
			CALIB_FIX_ASPECT_RATIO +
			//CALIB_ZERO_TANGENT_DIST +
		//	CALIB_USE_INTRINSIC_GUESS +
			CV_CALIB_FIX_INTRINSIC +
			//CALIB_SAME_FOCAL_LENGTH +
			//CALIB_RATIONAL_MODEL +
			CALIB_FIX_K3 + CALIB_FIX_K4 + CALIB_FIX_K5,
			TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 100, 1e-5));


		//CALIBRATION QUALITY CHECK
			// because the output fundamental matrix implicitly
			// includes all the output information,
			// we can check the quality of calibration using the
			// epipolar geometry constraint: m2^t*F*m1=0
			double err = 0;
			int npoints = 0;
			vector<Vec3f> lines[2];
			for (int i = 0; i < objectPoints.size(); i++)
			{
				int npt = (int)imagePointsRGB.size();
				Mat imgpt[2];
				imgpt[0] = Mat(imagePointsRGB[i]);
				imgpt[1] = Mat(imagePointsIR[i]);
				undistortPoints(imgpt[0], imgpt[0], cameraMatrixRGB, distCoeffsRGB, Mat(), cameraMatrixRGB);
				undistortPoints(imgpt[1], imgpt[1], cameraMatrixIR, distCoeffsIR, Mat(), cameraMatrixIR);
				computeCorrespondEpilines(imgpt[0], 1, fundamentalMat, lines[0]);
				computeCorrespondEpilines(imgpt[1], 2, fundamentalMat, lines[1]);

			
				for (int j = 0; j < npt; j++)
				{
					double errij = fabs(imagePointsRGB[i][j].x*lines[1][j][0] +
						imagePointsRGB[i][j].y*lines[1][j][1] + lines[1][j][2]) +
						fabs(imagePointsIR[i][j].x*lines[0][j][0] +
						imagePointsIR[i][j].y*lines[0][j][1] + lines[0][j][2]);
					err += errij;
				}
				npoints += npt;
			}
			avgEpipolarErr = err / npoints;
			cout << "average epipolar err = " << avgEpipolarErr << endl;

			return true;
	}
	else {

		cerr << "Stereo calibration for Kinect v2 currently only supported with initial intrinsic guess. Please change <Input_stereo_use_intrinsic_guess> to 1 in config.xml" << endl;
		return false;
	}

	

}

//! [board_corners]
static bool runCalibration( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                            vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
                            vector<float>& reprojErrs,  double& totalAvgErr)
{
    //! [fixed_aspect]
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if( s.flag & CALIB_FIX_ASPECT_RATIO )
        cameraMatrix.at<double>(0,0) = s.aspectRatio;
    //! [fixed_aspect]
    if (s.useFisheye) {
        distCoeffs = Mat::zeros(4, 1, CV_64F);
    } else {
        distCoeffs = Mat::zeros(8, 1, CV_64F);
    }

    vector<vector<Point3f> > objectPoints(1);
    calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
    double rms;

    if (s.useFisheye) {
        Mat _rvecs, _tvecs;
        rms = fisheye::calibrate(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, _rvecs,
                                 _tvecs, s.flag);

        rvecs.reserve(_rvecs.rows);
        tvecs.reserve(_tvecs.rows);
        for(int i = 0; i < int(objectPoints.size()); i++){
            rvecs.push_back(_rvecs.row(i));
            tvecs.push_back(_tvecs.row(i));
        }
    } else {
        rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
                              s.flag);
    }

    cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix,
                                            distCoeffs, reprojErrs, s.useFisheye);

    return ok;
}

// 	bool ok = runStereoCalibration(s, imageSize, cameraMatrixRGB, distCoeffsRGB, imagePointsRGB, cameraMatrixIR, distCoeffsIR, imagePointsIR, R, T, essentialMat, fundamentalMat, 
//rmsErr, avgEpipolarErr, useIntrinsicGuess);
//static void saveStereoCameraParams(Settings& s, Size& imageSize, Mat& cameraMatrixRGB, Mat& distCoeffsRGB,
//	Mat& cameraMatrixIR, Mat& distCoeffsIR,
//	Mat& R, Mat& T, Mat& R1, Mat& R2, Mat& P1, Mat& P2, Mat& Q, Mat& essentialMat, Mat& fundamentalMat,
//	double rmsErr, double avgEpipolarErr) {

static void saveStereoCameraParams(Settings& s, Size& imageSize, Mat& cameraMatrixRGB, Mat& distCoeffsRGB,
	Mat& cameraMatrixIR, Mat& distCoeffsIR,
	Mat& R, Mat& T, Mat& essentialMat, Mat& fundamentalMat,
	double rmsErr, double avgEpipolarErr, bool rectified = false, Mat& R1 = Mat(), Mat& R2 = Mat(), Mat& P1 = Mat(), Mat& P2 = Mat(), Mat& Q = Mat()) {


	FileStorage fs(s.outputFileName, FileStorage::WRITE);
	char buf[1024];
	//strftime(buf, sizeof(buf), "%c", t2);

	fs << "image_width" << imageSize.width;
	fs << "image_height" << imageSize.height;

	fs << "board_width" << s.boardSize.width;
	fs << "board_height" << s.boardSize.height;
	fs << "square_size" << s.squareSize;

	if (s.flag & CALIB_FIX_ASPECT_RATIO)
		fs << "fix_aspect_ratio" << s.aspectRatio;

	if (s.flag)
	{		
		sprintf(buf, "flags:%s%s%s%s",
		s.flag & CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "",
		s.flag & CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "",
		s.flag & CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "",
		s.flag & CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "");
		cvWriteComment(*fs, buf, 0);
	}

	fs << "flags" << s.flag;


	fs << "camera_matrix_rgb" << cameraMatrixRGB;
	fs << "distortion_coefficients_rgb" << distCoeffsRGB;

	fs << "camera_matrix_ir" << cameraMatrixIR;
	fs << "distortion_coefficients_ir" << distCoeffsIR;

	fs << "R" << R;
	fs << "T" << T;
	fs << "essential_matrix" << essentialMat;
	fs << "fundamental_matrix" << fundamentalMat;
	fs << "rms_error" << rmsErr;
	fs << "avg_epipolar_error" << avgEpipolarErr;

	if (rectified) {
		fs << "R1" << R1;
		fs << "R2" << R2;
		fs << "P1" << P1;
		fs << "P2" << P2;
		fs << "Q" << Q;
	}

	


}
	


// Print camera parameters to the output file
static void saveCameraParams( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                              const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                              const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
                              double totalAvgErr )
{
    FileStorage fs( s.outputFileName, FileStorage::WRITE );

    time_t tm;
    time( &tm );
    struct tm *t2 = localtime( &tm );
    char buf[1024];
    strftime( buf, sizeof(buf), "%c", t2 );

    fs << "calibration_time" << buf;

    if( !rvecs.empty() || !reprojErrs.empty() )
        fs << "nr_of_frames" << (int)std::max(rvecs.size(), reprojErrs.size());
    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    fs << "board_width" << s.boardSize.width;
    fs << "board_height" << s.boardSize.height;
    fs << "square_size" << s.squareSize;

    if( s.flag & CALIB_FIX_ASPECT_RATIO )
        fs << "fix_aspect_ratio" << s.aspectRatio;

    if (s.flag)
    {
        if (s.useFisheye)
        {
            sprintf(buf, "flags:%s%s%s%s%s%s",
                     s.flag & fisheye::CALIB_FIX_SKEW ? " +fix_skew" : "",
                     s.flag & fisheye::CALIB_FIX_K1 ? " +fix_k1" : "",
                     s.flag & fisheye::CALIB_FIX_K2 ? " +fix_k2" : "",
                     s.flag & fisheye::CALIB_FIX_K3 ? " +fix_k3" : "",
                     s.flag & fisheye::CALIB_FIX_K4 ? " +fix_k4" : "",
                     s.flag & fisheye::CALIB_RECOMPUTE_EXTRINSIC ? " +recompute_extrinsic" : "");
        }
        else
        {
            sprintf(buf, "flags:%s%s%s%s",
                     s.flag & CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "",
                     s.flag & CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "",
                     s.flag & CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "",
                     s.flag & CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "");
        }
        cvWriteComment(*fs, buf, 0);
    }

    fs << "flags" << s.flag;

    fs << "fisheye_model" << s.useFisheye;

    fs << "camera_matrix" << cameraMatrix;
    fs << "distortion_coefficients" << distCoeffs;

    fs << "avg_reprojection_error" << totalAvgErr;
    if (s.writeExtrinsics && !reprojErrs.empty())
        fs << "per_view_reprojection_errors" << Mat(reprojErrs);

    if(s.writeExtrinsics && !rvecs.empty() && !tvecs.empty() )
    {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for( size_t i = 0; i < rvecs.size(); i++ )
        {
            Mat r = bigmat(Range(int(i), int(i+1)), Range(0,3));
            Mat t = bigmat(Range(int(i), int(i+1)), Range(3,6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        //cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "extrinsic_parameters" << bigmat;
    }

    if(s.writePoints && !imagePoints.empty() )
    {
        Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
        for( size_t i = 0; i < imagePoints.size(); i++ )
        {
            Mat r = imagePtMat.row(int(i)).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "image_points" << imagePtMat;
    }
}

//! [run_and_save]
bool runCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                           vector<vector<Point2f> > imagePoints)
{
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(s, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs, reprojErrs,
                             totalAvgErr);
    cout << (ok ? "Calibration succeeded" : "Calibration failed")
         << ". avg re projection error = " << totalAvgErr << endl;

    if (ok)
        saveCameraParams(s, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, reprojErrs, imagePoints,
                         totalAvgErr);
    return ok;
}

bool runStereoCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrixRGB, Mat& distCoeffsRGB,
	vector<vector<Point2f> > imagePointsRGB, Mat& cameraMatrixIR, Mat& distCoeffsIR,
	vector<vector<Point2f> > imagePointsIR, bool useIntrinsicGuess)
{
	Mat R, T;
	Mat essentialMat, fundamentalMat;

	vector<float> reprojErrs;
	//double totalAvgErr = 0;
	double rmsErr = 0;
	double avgEpipolarErr = 0;

	bool ok = runStereoCalibration(s, imageSize, cameraMatrixRGB, distCoeffsRGB, imagePointsRGB, cameraMatrixIR, distCoeffsIR, imagePointsIR, R, T, essentialMat, fundamentalMat, 
		rmsErr, avgEpipolarErr, useIntrinsicGuess);

	if (ok) {
		cout << "Stereo calibration succeeded" << endl;
		cout << ". RMS  error = " << rmsErr << ". avgEpipolarErr  error = " << avgEpipolarErr << endl;
	}
	else {
		cout << "Stereo calibration failed" << endl;
	}	

	if (ok) {
		// rectify the image planes
		//Mat R1, R2, P1, P2, Q;		
		/*stereoRectify(cameraMatrixRGB, distCoeffsRGB,
			cameraMatrixIR, distCoeffsIR,
			imageSize, R, T, R1, R2, P1, P2, Q,
			CALIB_ZERO_DISPARITY, 1, imageSize, &validRoi[0], &validRoi[1]);*/
		/*stereoRectify(cameraMatrixRGB, distCoeffsRGB,
			cameraMatrixIR, distCoeffsIR,
			imageSize, R, T, R1, R2, P1, P2, Q,
			0, 1, imageSize, &validRoi[0], &validRoi[1]);*/

		/*saveStereoCameraParams(s, imageSize, cameraMatrixRGB, distCoeffsRGB,
			cameraMatrixIR, distCoeffsIR,
			R, T, essentialMat, fundamentalMat,
			rmsErr, avgEpipolarErr, true, R1, R2, P1, P2, Q);*/

		// visualize rectified results
		
		/*initUndistortRectifyMap(cameraMatrixRGB, distCoeffsRGB, R1, P1, imageSize, CV_16SC2, rmapRGB[0], rmapRGB[1]);
		initUndistortRectifyMap(cameraMatrixIR, distCoeffsIR, R2, P2, Size(IR_COLUMNS, IR_ROWS), CV_16SC2, rmapIR[0], rmapIR[1]);
				
		int w, h;
		
		sf = 600. / MAX(imageSize.width, imageSize.height);
		w = cvRound(imageSize.width*sf);
		h = cvRound(imageSize.height*sf);
		rectifiedCanvas.create(h, w * 2, CV_8UC3);*/

		saveStereoCameraParams(s, imageSize, cameraMatrixRGB, distCoeffsRGB,
			cameraMatrixIR, distCoeffsIR,
			R, T, essentialMat, fundamentalMat,
			rmsErr, avgEpipolarErr);
		

	}

	return ok;
}

//! [run_and_save]
